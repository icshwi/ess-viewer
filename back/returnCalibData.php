<?php
$data = json_decode(file_get_contents('php://input'),true);
$path = "/var/www/html/ess-viewer/backend/calibTables/";
$fileName = $path . $data['file'];
$csvFile = file($fileName);
$data = [];
foreach ($csvFile as $line) {
    if ($line !== null) {
      $data[] = str_getcsv($line);
    }
}
echo json_encode($data);
?>
