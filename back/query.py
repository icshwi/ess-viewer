from flask import Flask, render_template, request, jsonify
from flask_cors import CORS, cross_origin
from ics import getChildren, getDocs, getPVs, getParents, getCables, getDevices, getCount, get_xmlFields, getDeviceListing, getDevicePVs, retrieveDesc
from rf_cable import getFileOverview, getData

COMPRESS_MIMTYPES = ['application/json']
COMPLRESS_LEVEL = 6
COMPRESS_MIN_SIZE = 500
app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

def configure_app(app):
    Compress(app)

@app.route('/q_get_children', methods=['GET'])
def q_get_children():
    if request.method =='GET':
        nodes = getChildren(request.args['nodeFind'], request.args['withNames'], request.args['withID'])
        status = nodes['status']
        fbs = nodes['nodes']
        response = jsonify({'status': status, 'fbs': fbs})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response

@app.route('/q_get_docs', methods=['POST'])
def q_get_docs():
    if request.method =='POST':
        data = request.get_json()
        docs = getDocs(data['nodeFind'],data['download'], data['titleOnly'])
        response = jsonify({'status': docs['status'], 'list': docs['list']})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response

@app.route('/q_get_devices', methods=['POST'])
def q_get_devices():
    if request.method == 'POST':
        data = request.get_json()
        devices = getDevices(data['nodeFind'], data['withTypeDesc'], data['withFBS'])
        response = jsonify({'status': devices['status'], 'list': devices['list']})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response

@app.route('/q_retrieve_desc', methods=['GET'])
def q_retrieve_desc():
    if request.method == "GET":
        nodeFind = request.args['tag']
        desc = retrieveDesc(nodeFind)
        response = jsonify({'desc': desc})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response

@app.route('/q_get_device_listing', methods=['GET'])
def q_get_device_listing():
    if request.method == 'GET':
        nodeFind = request.args['nodeFind']
        devices = getDeviceListing(nodeFind)
        response = jsonify({'devices': devices})
        return response

@app.route('/q_get_device_pvs', methods=['GET'])
def q_get_device_pvs():
    if request.method == 'GET':
        device = request.args['device']
        pvs = getDevicePVs(device)
        response = jsonify({'pvs': pvs})
        return response

@app.route('/q_get_pvs', methods=['GET'])
def q_get_pvs():
    if request.method == 'GET':
        nodeFind = request.args['nodeFind']
        onlyArc = request.args['onlyArc']
        pvs = getPVs(nodeFind, onlyArc)
        response = jsonify({'pvs': pvs})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response

@app.route('/q_get_parents', methods=['GET'])
def q_get_parents():
    if request.method == 'GET':
        nodeFind = request.args['nodeFind']
        parents = getParents(nodeFind)
        response = jsonify({'parents': parents})    
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response

@app.route('/q_get_cables', methods=['POST'])
def q_get_cables():
    if request.method == 'POST':
        data = request.get_json()
        cables = getCables(data['nodeFind'])
        cntCables = cables.count('=ESS') - cables.count('Not found.')
        response = jsonify({'cables': cables, 'len': cntCables})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
    
@app.route('/q_get_cable_calib', methods=['GET'])
def q_get_cable_calib():
    if request.method == 'GET':
        filename = request.args['filename']
        return jsonify(getFileOverview(filename))

@app.route('/q_get_cable_dataset', methods=['GET'])
def q_get_cable_dataset():
    if request.method == 'GET':
        filename = request.args['filename']
        dataset = request.args['dset']
        return jsonify(getData(filename, dataset))

@app.route('/q_get_xml_fields', methods=['POST'])
def q_get_xml_fields():
    if request.method == 'POST':
        data = request.get_json()
        list_tags = data['list_tags']
        rootNode = data['rootNode']
        withISO = data['withISO']
        withLBS = data['withLBS']
        withLBS_Desc = data['withLBS_Desc']
        return jsonify(get_xmlFields(list_tags, rootNode, withISO, withLBS, withLBS_Desc))

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
