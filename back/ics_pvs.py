from channelfinder import ChannelFinderClient
from epicsarchiver import ArchiverAppliance
import urllib3
import subprocess
import json
from epics import caget

realNames={'jerzyjamroz': 'Jerzy Jamroz', 'thomasfay': 'Thomas Fay', 'gabrielfedel': 'Gabriel Fedel', 'saeedhaghtalab': 'Saeed Haghtalab', 'alfiorizzo': 'Alfio Rizzo', 'joaopaulomartins': 'Joao Paulo Martins', 'anirbanbhattacharyya': 'Anirban Bhattacharyya', 'rafaelmontano': 'Rafael Montano','yongksin': 'Yong Kian Sin', 'krisztianloki': 'Krisztian Loki', 'johnsparger': 'John Sparger', 'karlvestin': 'Karl Vestin','miklosboros': 'Miklos Boros', 'douglasbezerra': 'Douglas Bezerra Beniz', 'nicklasholmberg2': 'Nicklas Holmberg', 'fayechicken': 'Faye Chicken', 'emilioasensiconejero': 'Emilio Ansensi Conejero'}

def getDetails(pv):
    details = {'status': '', 'DESC': '','SYS': '', 'host': '', 'archStat': '', 'devType': '', 'contact': '', 'fieldDESC': '', 'fieldRTYP': '', 'fieldMeta': ''}
    # Check if PV is valid
    # Exception for quoted PVs
    if pv[0] == "'" and pv[-1] == "'":
        checkLen = 0
        checkColons = 0 
        checkLenP = 0
        pv = pv[1:-1]
    else:
        checkLen = len(pv) < 3 or len(pv) > 60
        checkColons=''
        checkLenP=''
        if not checkLen:
            checkColons = pv.count(':') != 2
        if not checkLen and not checkColons:
            checkLenP = pv[pv.find(':') + 1:]
            checkLenP = checkLenP[checkLenP.find(':') + 1:]
            checkLenP = len(checkLenP) > 20 or len(checkLenP) < 1
    # Only check length currently as too many online PVs are invalid
    # if checkLen or checkColons or checkLenP: 
    if checkLen: 
        reason = ''
        if checkLen:
            reason += 'Bad PV length. '
        if checkColons:
            reason += 'Must have exactly two colon chars. '
        if checkLenP:
            reason += 'Property length invalid. Must be between 1 and 20 characters'
        details['status'] = "Invalid PV name. " + reason
        return details

    url_cf = "http://channelfinder.tn.esss.lu.se"
    url_cs = 'https://csentry.esss.lu.se/api/v1'
    cs_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1OTEwMDY3NTEsIm5iZiI6MTU5MTAwNjc1MSwianRpIjoiMTBkYWM3Y2MtM2UzNi00ZjBhLWIyNTctMWU5YmE1NjczOTRkIiwiaWRlbnRpdHkiOjIwLCJmcmVzaCI6ZmFsc2UsInR5cGUiOiJhY2Nlc3MifQ.rKXbr66DIDdL58Gu20P07WXdI1JN8trJy15glI4UdEM'

    urllib3.disable_warnings()
    try:
        cf = ChannelFinderClient(BaseURL=url_cf)
    except:
        details['status'] = "Could not connect to channel finder"    
    res = cf.find(name=pv)
    if len(res) == 0:
        details['status'] = "Could not match PV in ChannelFinder."
        return details
    
    #Expect only a single result
    pv_prop = res[0]['properties']
    iocHost = ''
    ipAddr = ''
    for el in pv_prop:
        if el['name'] == 'hostName':
            fqHost = el['value']
            # Attempt to handle cases where ChannelFinder population was with localdomain
            fqHost = fqHost.replace('.localdomain', '')
            iocHost = fqHost.replace('.tn.esss.lu.se','')
        elif el['name'] == 'DESCRIPTION':
            details['DESC'] = el['value']
        elif el['name'] == 'SYSTEM':
            details['SYS'] = el['value']
        elif el['name'] == 'iocid':
            ipAddr = el['value']
            ipAddr = ipAddr[:ipAddr.find(':')]
    url_cs_view='https://csentry.esss.lu.se/network/hosts/view/'
    details['host'] = "<a href=" + url_cs_view + iocHost + ">" + iocHost + "</a> (" + ipAddr + ')'
           
    try:
        archiver = ArchiverAppliance("archiver-01.tn.esss.lu.se")   
    except:
        details['status'] = "Could not connect to channel archiver."
        return details
    archStat = archiver.get_pv_status(pv)
    if len(archStat) > 0:
        if archStat[0]['status'] == "Being archived":
            appliance = archStat[0]['appliance']
            archStat = "<a href = http://" + appliance + ".tn.esss.lu.se:17668/retrieval/ui/viewer/archViewer.html?pv=" + pv + ">Being archived</a>"
        else:
            archStat = archStat[0]['status']
        details['archStat'] = archStat 

    #Get CSEntry details
    cs_query = '/network/hosts/search?q=fqdn:"' + fqHost + '"'
    curlArgs = ['/usr/bin/curl', '--header', 'Authorization: Bearer ' + cs_token, url_cs + cs_query]
    x = subprocess.run(curlArgs, capture_output = True) 
    res = x.stdout
    s = json.dumps(res.decode('utf-8'))
    results = json.loads(s)
    results = json.loads(results)
    details['devType'] = results[0]['device_type']
    try:
        tmpContact = results[0]['ansible_vars']['vm_owner']
    except:
        try:
            tmpContact = [results[0]['user']]
        except:
            tmpContact = ['No contact person found.']
    contact = ''
    for name in tmpContact:
        try:
            contact += realNames[str(name)] + ', '
        except:
            contact += name + ', '
    if contact[-2:] == ', ':
        contact = contact[:-2]
    details['contact'] = contact
    details['status'] = "Request successful."
    rtyp = ""
    desc = caget(pv + '.DESC',timeout=1)
    rtyp = caget(pv + '.RTYP', timeout=1)
    asg  = caget(pv + '.ASG', timeout=1)
    if desc is None:
        descFormatted = "<b>DESC</b> : Not found.<br>"
    elif len(desc) == 0:
        descFormatted = "<b>DESC</b> : Not defined.<br>"
    else:
        descFormatted = "<b>DESC</b> : " + desc + "<br>"
    if rtyp is None:
        rtypFormatted = "<b>RTYP</b> : Not found.<br>"
    else:
        rtypFormatted = "<b>RTYP</b> : " + rtyp + "<br>"
    if asg is None:
        asgFormatted = "<b>ASG</b> : Not defined.<br>"
    else:
        asgFormatted = "<b>ASG</b> : " + asg + "<br>"

    analogues = ["ai", "ao", "waveform", "calcout", "compress"]
    digitals = ["bi", "bo"] 
    multibits = ["mbbi", "mbbo"]
    meta = ''
    listFields = ["DESC"]
    if rtyp is not None and len(rtyp) >= 2:
        if rtyp in analogues:
            listFields = ["EGU"]
        elif rtyp in digitals:
            listFields = ["ZNAM", "ONAM"]
        elif rtyp in multibits:
            listFields = {"ZRST" , "ONST", "TWST", "THST", "FRST", "FVST", "SXST", "SVST", "SVST", "EIST", "NIST", "TEST", "ELST", "TVST", "TTST", "FTST", "FFST"}
        fieldDesc = ''
        for i, el in enumerate(listFields):
            tmp = caget(pv + '.' + el, timeout = 1)
            if tmp is None:
                tmp = "Not found."
            elif len(tmp) == 0:
                tmp = "Not defined."
            if rtyp in multibits:
                if i < 10:
                    fieldDesc = "<b>Bit 0" + str(i) + "</b>"
                else:
                    fieldDesc = "<b>Bit " + str(i) + "</b>"
            elif rtyp in analogues:
                fieldDesc = "<b>EGU</b>"
            elif rtyp in digitals:
                if el == "ZNAM":
                    fieldDesc = "<b>ZNAM</b>"
                elif el == "ONAM":
                    fieldDesc = "<b>ONAM</b>"
            else:
                fieldDesc = el
            if tmp is not None:
                meta += fieldDesc + ": " + tmp + "<br>"
            else:
                meta += fieldDesc + ": No description.<br>"
    details['fieldMeta'] = descFormatted + rtypFormatted + asgFormatted + meta
    return details


# tokens: list of valid tokens
# selectedToken: the token to extract value for
def extractTokenVal(tokens, selectedToken, pattern):
    posStart = pattern.find(selectedToken) 
    if posStart > 0:
        tmp = pattern[posStart+1:]    
    else:
        return ""
    posEnd = len(tmp)
    for char in tokens:
        if char != selectedToken:
           tmpStart = tmp.find(char)
           if tmpStart > 0 and tmpStart < posEnd:
                posEnd = tmp.find(char)
    val = tmp[:posEnd]
    return val

# Removes all tokens and values from pattern
def getPattern(tokens, pattern):
    for char in tokens:
        charPos = pattern.find(char)
        if charPos > 0:
            pattern = pattern[:charPos]    
    return pattern

def getName(pv):
    return pv['name']

def getProp(listProp, incAll, incSys, incDesc, incCh):
    strOut = ""
    if incAll:
        for prop in listProp:
            strOut += ', <b>' + prop['name'] + '</b>: ' + prop['value']
    else:
        for prop in listProp:
            if incSys:
                if prop['name'] == "SYSTEM":
                    strOut += ', <b>' + prop['name'] + '</b>: ' + prop['value']
            if incDesc:
                if prop['name'] == "DESCRIPTION":
                    strOut += ', <b>' + prop['name'] + '</b>: ' + prop['value']
            if incCh:
                if prop['name'] == "HW-Channel":
                    strOut += ', <b>' + prop['name'] + '</b>: ' + prop['value']
    return strOut
def getTagVars(tagMatch):
    if tagMatch.count("~") != 1 and tagMatch.count("=") != 1:
        return ["","",1]
    else:
        if tagMatch.count("~") == 1:
            posEnd = tagMatch.find("~")
            exactMatch = False
        else:
            posEnd = tagMatch.find("=")
            exactMatch = True

        key = tagMatch[1:posEnd] + "</b>:"
        matchVal = tagMatch[posEnd+1:]

        return [key,matchVal,exactMatch]

def matchProp(propMatch, PV):
    # Strip away extra meta-data
    posMeta = PV.find("<b>")
    if posMeta > 0:
        PV = PV[:posMeta-2]
    if len(propMatch) == 0:
        return True

    if PV.count(':') != 2:
        return False
    pos1=PV.find(':')
    pos2=PV[pos1+1:].find(':')
    
    prop = PV[pos1 + pos2 + 2:]
    match = False
    for el in propMatch:
        if el.upper() in prop.upper():
            match = True
        else:
            match = False
            break
    return match
    
def matchTag(tagKey, tagMatchVal, PV, exactMatch):
    if len(tagKey) == 0 and len(tagMatchVal) == 0:
        return True

    if tagKey in PV:
        # Strip down string to just the tag value
        posStart = PV.find(tagKey) + len(tagKey) + 1 
        tagVal = PV[posStart:]
        # Bold tag servers as meta-data delimiter 
        posBold = tagVal.find("<b>")
        if posBold > 0:
            tagVal = tagVal[:posBold-2]
        if exactMatch:
            match = (tagMatchVal == tagVal)
        else:
            match = (tagMatchVal.upper() in tagVal.upper())
    else:
        match = False
    return match

def matchAll(matchAll, PV):
    if len(matchAll) == 0:
        return True
    if matchAll.upper() in PV.upper():
        return True
    else:
        return False

def findChannels(pattern, incDesc, incSys, incCh, incAll, showInactive, useLab):
    channels = {'status': 'status', 'matches': ''}
    if len(pattern) < 3: 
        channels['status'] = "Pattern too short."
        return channels
    # Special query characters
    tokens = ['&', '%', '|']
    #Input checking
    for char in tokens:
        cntSpecial = pattern.count(char)
        if cntSpecial > 1:
            channels['status'] = "Invalid query."
            channels['matches'] = "No matched PVs."
            return channels
    propMatch = extractTokenVal(tokens, '&', pattern)
    propMatch = propMatch.split(',')
    tagMatch = extractTokenVal(tokens, '%', pattern)
    grepMatch = extractTokenVal(tokens, '|', pattern)
    pattern = getPattern(tokens,pattern)
    tagKey, tagMatchVal, tagExactMatch = getTagVars(tagMatch)
    # Maybe some error checking of input
    url_cf = "http://channelfinder.tn.esss.lu.se"
    if useLab:
        url_cf = "http://channelfinder-lab.cslab.esss.lu.se"
    urllib3.disable_warnings()
    try:
        cf = ChannelFinderClient(BaseURL=url_cf)
    except:
        details['status'] = "Could not connect to channel finder"    

    for el in propMatch:
        if len(el) > 0 and len(el)< 2:
            channels['status'] = "Match: '" + propMatch + "' is too short. Must be at least two chars."
            channels['matches'] = "No channel matches."
            return channels
    # Tag-matching
    if "SYSTEM" in tagMatch:
        incSys = True
    elif "DESCRIPTION" in tagMatch:
        incDesc = True
    elif "HW-Channel" in tagMatch:
        incCh = True

    matchTags = len(tagMatch) > 0
    # Option to have %TAG=val syntax instead of %TAG: val
    tagMatch = tagMatch.replace("=",": ")
    tagMatch = tagMatch.replace(": ","</b>: ")
    res = cf.find(name=pattern)

    strOut = ''
    listCH = []
    for el in res:
        if not showInactive:
            isActive = False
            for prop in el['properties']:
                if prop['name'] == "pvStatus":
                    isActive = prop['value'] == "Active"
                    break
        if isActive or showInactive:
            strOut = getName(el)
            if len(strOut) > 0:
                strOut += getProp(el['properties'], incAll, incSys, incDesc, incCh)
                listCH.append(strOut)

    listOutput = []
    if len(listCH) > 0:
        for el in listCH:
            incProp = matchProp(propMatch, el)
            incTag = matchTag(tagKey, tagMatchVal, el, tagExactMatch)
            incAll = matchAll(grepMatch,el)
            incActive = False
            inc = incProp and incTag and incAll
            if inc:
                listOutput.append(el)

        if len(listOutput) == 1:
            channels['status'] = "One matched PV."
        elif len(listOutput) > 1:
            channels['status'] = str(len(listOutput)) + " matched PVs."
        else:
            channels['status'] = "No matched PVs."
    else:
        channels['status'] = "No matched PVs."
    channels['matches'] = listOutput
    return channels
