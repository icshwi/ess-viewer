import h5py

def getFileOverview(filename):
    path = "/home/thomasfay/hdf5-test/"
    f = h5py.File(path + filename)
    keys = f.keys()
    listKeys = []
    for dset in keys:
        att = f[dset].attrs
        for a in att:
            x_units = att['X Units']
            y_units = att['Y Units']

        x,y = f[dset].shape
        listKeys.append(dset)
    listAttrs = []
    for el in f.attrs:
        listAttrs.append(el + ": " + str(f.attrs[el]))

    return listKeys,listAttrs

def getData(filename, dataset):
    path = "/home/thomasfay/hdf5-test/"
    f = h5py.File(path + filename)
    dset = f[dataset]
    x_units = dset.attrs['X Units']
    y_units = dset.attrs['Y Units']
    x_vals = []
    y_vals = []
    for el in dset:
        x_vals.append(el[0])
        y_vals.append(el[1])


    attrs_keys = []
    attrs_vals = []
    for a in dset.attrs:
        attrs_keys.append(a)
        if dset.attrs[a] != dset.attrs[a]:
            attrs_vals.append("NaN")
        else:
            attrs_vals.append(dset.attrs[a])
    return x_vals,y_vals, x_units, y_units, attrs_keys, attrs_vals
