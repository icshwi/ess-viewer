import requests
import json
import xml.etree.ElementTree as ET

def getDevices(pattern, showDevices, includeDesc, includeName, showActive, includeStatus):
    devices = {'status': 'no response', 'devices': ''}
    if '&' in pattern:
        pos = pattern.find('&')
        nameMatch = pattern[pos+1:]
        pattern = pattern[:pos]
    else:
        nameMatch = ''
    url_naming = "https://naming.esss.lu.se"
    naming_query = "/rest/parts/mnemonicPath/search/" + pattern

    # fetch match
    response = requests.get(url_naming + naming_query) 
    root = ET.fromstring(response.content)
    for el in root:
        includePart = False
        type = ""
        stat = ""

        if el.find('type') is not None:
            if el.find('type').text is not None:
                type = el.find('type').text 
        if el.find('status') is not None:
            if el.find('status').text is not None:
                stat = el.find('status').text
        
        activeStatuses = {"Approved", "Pending"}
        includeType = (type == "Device Structure" and showDevices) or (type == "System Structure" and not showDevices)
        includeActive = (stat in activeStatuses and showActive) or not showActive
        includePart = includeType and includeActive

        if includePart:
            desc = ", <b>Description: </b>None"
            if el.find('mnemonic') is not None:
                if el.find('mnemonic').text is not None:
                    mnemonic = el.find('mnemonic').text
            if includeDesc:
                if el.find('description') is not None:
                    if el.find('description').text is not None:
                        desc = ", <b>Description: </b>" + el.find('description').text
                    else:
                        desc = ", <b>Description: </b> Not found."
            else:
                desc = ""
            if includeName:
                if el.find('name') is not None:
                    if el.find('name').text is not None:
                        name = ", <b>Name: </b>" + el.find('name').text
                    else:
                        name = ", <b>Name: </b> Not found."
            else:
                name = ""
            if includeStatus:
                if el.find('status') is not None:
                    if el.find('status').text is not None:
                        status = ", <b>Status: </b>" + el.find('status').text
                    else:
                        status = ", <b>Status: </b> Not Found."
            else:
                status = ""
            if len(nameMatch) == 0 or nameMatch.upper() in mnemonic.upper():
                devices['devices'] += mnemonic + desc + name + status + "<br>"

    partCount = devices['devices'].count("<br>")
    if devices['devices'][-4:] == "<br>":
        devices['devices'] = devices['devices'][:-4]
    if showDevices:
        if partCount == 1:
            matchPart = "1 device matched."
        else:
            matchPart = str(partCount) + " devices matched."
    else:
        if partCount == 1:
            matchPart = "1 system matched"
        else:
            matchPart = str(partCount) + " systems matched."
    devices['status'] = matchPart
    return devices
