from epics import caget
from channelfinder import ChannelFinderClient
import urllib3
import numpy

def getLookupTable(PV, calibPoints):
    lut = {'vals': [], 'desc': '', 'egu': ''}
    llrf = "RFS-DIG-" in PV
    if len(PV) < 10:
        return lut
    if calibPoints is None or len(calibPoints) < 2:
        return lut

    desc = caget(PV + '.DESC')
    if desc is not None:
        lut['desc'] = desc
    egu = caget(PV + '.EGU')
    if egu is not None:
        lut['egu'] = egu
    nord = caget(PV + '.NORD')
    try:
        nord = int(nord)
    except ValueError:
        nord = 65536
    if nord <2:
        vals = []
        for i in range(0,len(calibPoints)):
            vals.append(0)
        lut['vals'] = vals
        return lut

    vals = caget(PV, count = nord )
    valsReduced = []
    if len(vals) > 0:
        # Handle non-numerical end value gracefully 
        if numpy.isnan(vals[-1]):
            vals[-1] = vals[-2]
        
        for i,pnt in enumerate(calibPoints):
            if llrf:
                valsReduced.append(vals[i])
            else:
                valsReduced.append(vals[pnt])
        lut['vals'] = valsReduced
        return lut
    else:
        lut['vals'] = "Failed to retrieve lookup table."
        return lut

def getLutPVs(pattern):
    # Hack until RFQ IOC updated
    if "RFQ-010" in pattern:
        listPVs = [
            "RFQ-010:RFS-PAmp-110:PwrFwd:ScalingLut",
            "RFQ-010:RFS-Kly-110:PwrFwd:ScalingLut",
            "RFQ-010:RFS-Load-130:PwrFwd:ScalingLut",
            "RFQ-010:RFS-Kly-110:PwrRflct:ScalingLut",
            "RFQ-010:RFS-Cav-110:PwrFwd:ScalingLut",
            "RFQ-010:RFS-Cav-120:PwrFwd:ScalingLut",
            "RFQ-010:RFS-Load-120:PwrFwd:ScalingLut",
            "RFQ-010:RFS-Cav-110:Fld:ScalingLut",
            "RFQ-010:RFS-Cav-110:PwrRflct:ScalingLut",
            "RFQ-010:RFS-Cav-120:PwrRflct:ScalingLut",
            "RFQ-010:RFS-Mod-110:Cur:ScalingLut",
            "RFQ-010:RFS-Mod-110:Vol:ScalingLut",
            "RFQ-010:RFS-SolPS-110:Cur:ScalingLut",
            "RFQ-010:RFS-SolPS-120:Cur:ScalingLut",
            "RFQ-010:RFS-SolPS-130:Cur:ScalingLut",
            "RFQ-010:RFS-EPR-110:I:ScalingLut",
            "RFQ-010:RFS-EPR-120:I:ScalingLut",
            "RFQ-010:RFS-FIM-101:AI7:ScalingLut",
            "RFQ-010:RFS-FIM-101:AI8:ScalingLut",
            "RFQ-010:RFS-FIM-101:AI9:ScalingLut",
            "RFQ-010:RFS-DIG-101:AI0-CalEGU",
            "RFQ-010:RFS-DIG-101:AI1-CalEGU",
            "RFQ-010:RFS-DIG-101:AI2-CalEGU",
            "RFQ-010:RFS-DIG-101:AI3-CalEGU",
            "RFQ-010:RFS-DIG-101:AI4-CalEGU",
            "RFQ-010:RFS-DIG-101:AI5-CalEGU",
            "RFQ-010:RFS-DIG-101:AI6-CalEGU",
            "RFQ-010:RFS-DIG-101:AI7-CalEGU",
            "RFQ-010:RFS-DIG-101:AI8-CalEGU",
            "RFQ-010:RFS-DIG-101:AI9-CalEGU",
            "RFQ-010:RFS-DIG-102:AI0-CalEGU",
            "RFQ-010:RFS-DIG-102:AI1-CalEGU",
            "RFQ-010:RFS-DIG-102:AI2-CalEGU",
            "RFQ-010:RFS-DIG-102:AI3-CalEGU",
            "RFQ-010:RFS-DIG-102:AI4-CalEGU",
            "RFQ-010:RFS-DIG-102:AI5-CalEGU",
            "RFQ-010:RFS-DIG-102:AI6-CalEGU",
            "RFQ-010:RFS-DIG-102:AI7-CalEGU",
            "RFQ-010:RFS-DIG-102:AI8-CalEGU",
            "RFQ-010:RFS-DIG-102:AI9-CalEGU"
            ]
        return listPVs
    listPVs = []
    url_cf = "http://channelfinder.tn.esss.lu.se"
    urllib3.disable_warnings()
    try:
        cf = ChannelFinderClient(BaseURL=url_cf)
    except:
        details['status'] = "Could not connect to channel finder"    
    res = cf.find(name=pattern + 'RFS*ScalingLut')
    for el in res:
        if not el['properties'][0]['value'] == "Inactive":
            listPVs.append(el['name'])
    
    res = cf.find(name=pattern + '*RFS-DIG*CalEGU')
    for el in res:
        if not el['properties'][0]['value'] == "Inactive":
            listPVs.append(el['name'])
    
    return listPVs
