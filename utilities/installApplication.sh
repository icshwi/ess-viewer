installPath=/var/www/html/ess-viewer
scriptPath=$(realpath $0)
srcPath=$(dirname $scriptPath)
srcPath=$(dirname $srcPath)
username=$(whoami)
serverhost=$(hostname)
if [ ! -d $installPath ]; then
    sudo mkdir -p $installPath
    sudo chown $username $installPath
fi
## Front end pages
cp $srcPath/front/* $installPath
for el in $(ls $srcPath/front)
do
    eval "sed 's/{{ server_addr }}/$serverhost/g' < $srcPath/front/$el > $installPath/$el"
done

## Back End
# Python + Flask
backInstallPath=/var/www/html/ess-viewer/backend
backSrcPath=$srcPath/back/

mkdir -p $backInstallPath
mkdir -p $backInstallPath/calibTables
mkdir -p $backInstallPath/calibTablesBackup
chmod 777 $backInstallPath/calibTables
chmod 777 $backInstallPath/calibTablesBackup
cp $backSrcPath/*py $backInstallPath
for file in $(ls $backSrcPath | grep sh)
do
    eval "sed 's/{{ user }}/$username/g' < $backSrcPath/$file > $backInstallPath/$file"
done
cp $backSrcPath/*.php $backInstallPath/
